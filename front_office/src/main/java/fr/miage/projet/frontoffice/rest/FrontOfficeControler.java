/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.frontoffice.rest;

import fr.miage.projet.frontoffice.repos.CoursWithMembreRepository;
import fr.miage.projet.frontoffice.transientobj.Cours;
import fr.miage.projet.frontoffice.transientobj.CoursWithMembre;
import fr.miage.projet.frontoffice.transientobj.Membre;
import fr.miage.projet.frontoffice.transientobj.NiveauMaitrise;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Yanek
 */
@RestController("/")
public class FrontOfficeControler {

    private final static Logger LOG = LoggerFactory.getLogger(FrontOfficeControler.class);

    @Autowired
    CoursWithMembreRepository coursWithMembreRepository;

    @PostMapping
    void createCours(@RequestBody Cours cours) {
        LOG.info("Adding new cours: " + cours);
        Membre m = coursWithMembreRepository.getMembre(cours.getLoginEnseignant());

        if (!m.enseignant) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "Le membre doit être un enseignant.");
        }

        /**
         * La date d’un cours doit toujours être supérieure de 7 jours
         * calendaires par rapport à la date de saisie.
         */
        LocalDate now = LocalDate.now();
        if (cours.getDate().getYear() < now.getYear()
                || cours.getDate().getMonthValue() < now.getMonthValue()
                || cours.getDate().getDayOfYear() < now.getDayOfYear() + 7) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "La date de début du cours doit être supérieur de 7 jours a la date d'aujourd'hui.");
        }
        /*
         * Un cours ne peut être créé que par un enseignant « apte ».
         */
        if (!m.apte) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "L'enseignant doit être apte à faire du sport.");
        }

        /*
         * Ne peuvent être enseignants que les membres dont le niveau
         * d’expertise est strictement supérieur à celui ciblé par le cours.
         */
        if (cours.getNiveau().compareTo(m.getNiveau()) < 0) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "Le niveau du cours est trop élevé pour l'enseignant.");
        }

        coursWithMembreRepository.createCours(cours);
    }

    @PostMapping("participer")
    void participerCours(@RequestParam("idCours") String idCours, @RequestParam("login") String login) {

        Membre m = coursWithMembreRepository.getMembre(login);
        CoursWithMembre c = coursWithMembreRepository.getCours(idCours);
        /**
         * Un cours ne peut être suivi par un élève que si celui-ci est «apte».
         */
        if (!m.apte) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "L'élève doit être apte à faire du sport.");
        }
        switch (c.membreAllowed(m)) {
            case 0:
                coursWithMembreRepository.ajouterParticipant(idCours, m.login);
                break;
            case 1:
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "le cours est complet.");
            case 2:
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "La cours a déjà eu lieu.");
            case 3:
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "L'élève n'as pas le bon niveau.");
            case 4:
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "L'élève participe déjà au cours.");
            case 5:
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "Le membre est déja l'enseignant du cours.");
        }

    }

    /**
     * La liste de tous les cours disponibles pour un membre
     *
     * @param login login du membre pour lequel on veut les cours
     */
    @GetMapping("listerCoursMembre/{login}")
    Collection<Cours> listerCoursPourMembre(@PathVariable("login") String login) {
        Collection<Cours> ret = new ArrayList<>();
        Membre m = coursWithMembreRepository.getMembre(login);
        coursWithMembreRepository.getCours().stream().filter(c -> c.membreAllowed(m) == 0).forEach(ret::add);
        return ret;
    }

    /**
     * La liste de tous les cours disponibles pour un membre
     *
     * @param login login du membre pour lequel on veut les cours
     */
    @GetMapping("listerParticipants/{idCours}")
    Collection<Membre> listerParticipants(@PathVariable("idCours") String idCours) {
        return coursWithMembreRepository.getCours(idCours).getListeMembresParticipants();
    }

    /**
     * Retourne la liste des statistiques pour le président
     * "totCotisations": Total des cotisations pour l'année
     * "totCotisationsPayees": Total des cotisations déjà payées
     * "nbCours": Total de cours
     * "enseignant": Total des enseignants triés par niveaux
     * "nbMembres" : Nombre total de membres
     */
    @GetMapping("stats")
    Map<String, Object> getStatistiques() {
        Map<String, Object> ret = new HashMap<>();

        //On stock la liste de membre pour eviter de faire trop d'appels
        Collection<Membre> lMembres = coursWithMembreRepository.getMembres();

        //On recupere les enseignants
        Collection<Membre> lEns = lMembres.stream().filter(m -> m.isEnseignant()).collect(Collectors.toList());

        Map<String, Long> ens = new HashMap<>();
        for (NiveauMaitrise n : NiveauMaitrise.values()) {
            ens.put(
                    n.name(),
                    lEns.stream()
                            .filter(m -> m.niveau.equals(n))
                            .count()
            );
        }

        //On forge la réponse
        ret.put("nbMembres", lMembres.size());
        ret.put("enseignant", ens);
        ret.put("nbCours", (double) coursWithMembreRepository.getCours().size());
        ret.put("totCotisations", coursWithMembreRepository.getListeCotisations().stream().mapToDouble(d -> d).sum());
        ret.put("totCotisationsPayees", coursWithMembreRepository.getListeCotisationsPayees().stream().mapToDouble(d -> d).sum());
        return ret;
    }

    /**
     * @return le solde de l'application
     */
    @GetMapping("solde")
    Double getSolde() {
        return 10000d;
    }
}
