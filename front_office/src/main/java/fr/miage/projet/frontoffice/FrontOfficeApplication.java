package fr.miage.projet.frontoffice;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import fr.miage.projet.frontoffice.repos.CoursWithMembreRepository;
import fr.miage.projet.frontoffice.repos.CoursWithMembreRepositoryImpl;
import fr.miage.projet.frontoffice.transientobj.Lieu;
import fr.miage.projet.frontoffice.transientobj.LieuDeserializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class FrontOfficeApplication {

    public static final String MEMBRE_SERVICE_URL = "http://gestion-membres-1:8080/membres/";
    public static final String COURS_SERVICE_URL = "http://gestion-cours-1:8080/cours/";
    public static final String LIEU_SERVICE_URL = "https://data.toulouse-metropole.fr/api/v2/catalog/datasets/piscines/";

    public static void main(String[] args) {
        SpringApplication.run(FrontOfficeApplication.class, args);
    }

    @Bean
    public CoursWithMembreRepository coursWithMembreRepository() {
        return new CoursWithMembreRepositoryImpl(MEMBRE_SERVICE_URL, COURS_SERVICE_URL, LIEU_SERVICE_URL);
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate rest = new RestTemplate();
        rest.getMessageConverters().add(0, mappingJacksonHttpMessageConverter());
        return rest;
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(LieuObjectMapper());
        return converter;
    }

    @Bean
    public ObjectMapper LieuObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("CustomCarDeserializer", new Version(1, 0, 0, null, null, null));
        module.addDeserializer(Lieu.class, new LieuDeserializer());
        mapper.registerModule(module);
        return mapper;
    }
}
