package fr.miage.projet.frontoffice.repos;

import fr.miage.projet.frontoffice.transientobj.Cours;
import fr.miage.projet.frontoffice.transientobj.CoursWithMembre;
import fr.miage.projet.frontoffice.transientobj.Membre;
import java.util.Collection;

/**
 *
 * @author Yanek
 */
public interface CoursWithMembreRepository {

    /**
     * @param login
     * @return le membre au login passé en parametre
     */
    public Membre getMembre(String login);

    /**
     * Créer un cours
     * @param cours
     */
    public void createCours(Cours cours);

    /**
     * Retourne le cours voulu avec ses membres chargés
     *
     * @param idCours
     * @return le cours dont l'id est passé en parametre
     */
    public CoursWithMembre getCours(String idCours);

    /**
     * Ajoute le participant au login au cours à l'idcours
     * @param idCours
     * @param login
     */
    public void ajouterParticipant(String idCours, String login);

    /**
     * @return tous les cours avec leur membres chargés
     */
    public Collection<CoursWithMembre> getCours();

    /**
     * @return tous les membres
     */
    public Collection<Membre> getMembres();

    /**
     * @return tous les enseignants
     */
    public Collection<Membre> getEnseignants();

    /**
     * @return la listes de toutes les cotisations
     */
    public Collection<Double> getListeCotisations();

    /**
     * @return la listes de toutes les cotisations deja payées
     */
    public Collection<Double> getListeCotisationsPayees();
}
