/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.frontoffice.transientobj;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import java.util.Collection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yanek
 */
@NoArgsConstructor
@Getter
public class Cours {

    private final static Logger LOG = LoggerFactory.getLogger(Cours.class);

    protected static final Integer MAX_PARTICIPANT = 2;

    String idCours;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    LocalDate date;

    NiveauMaitrise niveau;

    String nom;

    String loginEnseignant;

    String idLieu;

    Integer dureeMinute;

    Collection<String> listeParticipants;

    public Integer membreAllowed(Membre m) {
        /**
         * Chaque cours peut être dispensé pour un maximum de 15 élèves. Pour
         * les tests, on se limitera à 2 élèves.
         */
        if (listeParticipants.size() >= MAX_PARTICIPANT) {
            return 1;
        }
        /**
         * throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED,
         * "L'élève doit être apte à faire du sport.");..
         */
        if (LocalDate.now().compareTo(this.date) > 0) {
            return 2;
        }
        /**
         * Ne peuvent s’inscrire à un cours que les membres dont le niveau
         * d’expertise est égal à celui ciblé par le cours.
         */
        LOG.info("Niveau cours : " + this.niveau + " niveau membre :" + m.getNiveau());
        if (!this.niveau.equals(m.getNiveau())) {
            return 3;
        }
        /**
         * Ne peut participer un membre deja participant
         */
        if (listeParticipants.contains(m.login)) {
            return 4;
        }
        if (loginEnseignant.equals(m.login)) {
            return 5;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Cours{" + "idCours=" + idCours + ", date=" + date + ", niveau=" + niveau + ", nom=" + nom + ", loginEnseignant=" + loginEnseignant + ", idLieu=" + idLieu + ", dureeMinute=" + dureeMinute + ", listeParticipants=" + listeParticipants + '}';
    }

}
