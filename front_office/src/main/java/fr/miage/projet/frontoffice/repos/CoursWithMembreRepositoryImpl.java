/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.frontoffice.repos;

import fr.miage.projet.frontoffice.transientobj.Cours;
import fr.miage.projet.frontoffice.transientobj.CoursWithMembre;
import fr.miage.projet.frontoffice.transientobj.Lieu;
import fr.miage.projet.frontoffice.transientobj.Membre;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Yanek
 */
public class CoursWithMembreRepositoryImpl implements CoursWithMembreRepository {

    private final static Logger LOG = LoggerFactory.getLogger(CoursWithMembreRepositoryImpl.class);

    @Autowired
    protected RestTemplate restTemplateMembre;

    protected String serviceUrlMembre;

    @Autowired
    protected RestTemplate restTemplateCours;

    protected String serviceUrlCours;

    @Autowired
    protected RestTemplate restTemplateLieu;

    protected String serviceUrlLieu;

    public CoursWithMembreRepositoryImpl(String serviceUrlMembre, String serviceUrlCours, String serviceUrlLieu) {
        this.serviceUrlMembre = serviceUrlMembre;
        this.serviceUrlCours = serviceUrlCours;
        this.serviceUrlLieu = serviceUrlLieu;
    }

    @Override
    public Membre getMembre(String login) {
        LOG.info("Getting membre with login :" + login + " at " + serviceUrlMembre + "/" + login);
        return restTemplateMembre.getForObject(serviceUrlMembre + "{id}", Membre.class, login);
    }

    @Override
    public CoursWithMembre getCours(String idCours) {
        Cours c = restTemplateCours.getForObject(serviceUrlCours + "{id}", Cours.class, idCours);
        return getCoursWithMembreFromCours(c);
    }

    @Override
    public void createCours(Cours cours) {
        //TODO add verif for lieu
        restTemplateCours.postForObject(serviceUrlCours, cours, Cours.class);
    }

    @Override
    public void ajouterParticipant(String idCours, String login) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);

        HttpEntity<String> entity = new HttpEntity<>(login, headers);

        restTemplateCours.exchange(serviceUrlCours + "participer/{id}", HttpMethod.PUT, entity, String.class, idCours);
    }

    @Override
    public Collection<CoursWithMembre> getCours() {
        Cours[] cours = restTemplateCours.getForObject(serviceUrlCours, Cours[].class);
        return Arrays.stream(cours).map(this::getCoursWithMembreFromCours).collect(Collectors.toList());
    }

    private CoursWithMembre getCoursWithMembreFromCours(Cours c) {
        CoursWithMembre cwm = new CoursWithMembre();
        cwm.setAll(c);
        c.getListeParticipants().stream().map(login -> getMembre(login)).forEach(membre -> cwm.getListeMembresParticipants().add(membre));

        LOG.info("Getting lieu at " + serviceUrlLieu + "records/" + c.getIdLieu());
        Lieu l = restTemplateLieu.getForObject(serviceUrlLieu + "records/{id}", Lieu.class, c.getIdLieu());
        cwm.setLieu(l);

        return cwm;
    }

    @Override
    public Collection<Membre> getMembres() {
        Membre[] membres = restTemplateMembre.getForObject(serviceUrlMembre, Membre[].class);
        return Arrays.asList(membres);
    }

    @Override
    public Collection<Membre> getEnseignants() {
        Membre[] membres = restTemplateMembre.getForObject(serviceUrlMembre + "enseignants", Membre[].class);
        return Arrays.asList(membres);
    }

    @Override
    public Collection<Double> getListeCotisations() {
        Double[] cotisation = restTemplateMembre.getForObject(serviceUrlMembre + "cotisations", Double[].class);
        return Arrays.asList(cotisation);
    }

    @Override
    public Collection<Double> getListeCotisationsPayees() {
        Double[] cotisation = restTemplateMembre.getForObject(serviceUrlMembre + "cotisations/payees", Double[].class);
        return Arrays.asList(cotisation);
    }
}
