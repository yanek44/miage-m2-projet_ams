/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.frontoffice.transientobj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

/**
 *
 * @author Yanek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class Lieu {
    String nom;
    String adresse;
    String saison;
}
