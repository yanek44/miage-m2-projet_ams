/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.frontoffice.transientobj;

/**
 * Represente les différents niveau de maitrise pour un membre ou un cours
 *
 * @author Yanek
 */
public enum NiveauMaitrise {
    Niveau1, Niveau2, Niveau3;
}
