/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.frontoffice.transientobj;

import java.util.ArrayList;
import java.util.Collection;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Yanek
 */
@Getter
public class CoursWithMembre extends Cours {

    Collection<Membre> listeMembresParticipants;

    @Setter
    Lieu lieu;

    public CoursWithMembre() {
        super();
        this.listeMembresParticipants = new ArrayList<>();
    }

    public void setAll(Cours c) {
        this.date = c.date;
        this.idCours = c.idCours;
        this.idLieu = c.idLieu;
        this.listeParticipants = c.listeParticipants;
        this.loginEnseignant = c.loginEnseignant;
        this.niveau = c.niveau;
        this.dureeMinute = c.dureeMinute;
        this.nom = c.nom;
    }
}
