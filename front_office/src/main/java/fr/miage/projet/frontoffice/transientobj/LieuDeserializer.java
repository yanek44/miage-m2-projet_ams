/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.frontoffice.transientobj;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

/**
 * Deserializer permettant de transformer la réponse de l'api de toulouse en
 * objet Lieu
 *
 * @author Yanek
 */
public class LieuDeserializer extends StdDeserializer<Lieu> {

    public LieuDeserializer() {
        this(null);
    }

    public LieuDeserializer(Class<?> c) {
        super(c);
    }

    @Override
    public Lieu deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        Lieu l = new Lieu();
        //Get root node named record
        JsonNode node = (JsonNode) jp.getCodec().readTree(jp).get("record");

        //Get fields node containing rest of the fields
        JsonNode fieldNode = node.get("fields");

        l.adresse = fieldNode.get("adresse").asText();
        l.nom = fieldNode.get("nom_complet").asText();
        l.saison = fieldNode.get("saison").asText();
        
        return l;
    }

}
