# miage-m2-projet_ams

Application permettant la mise a l'eau de Miagistes !

Projet de M2 MIAGE ayant pour but de mettre en place une architecture micros-services en Spring.

Pour lancer le projet, allez dans ".\src\main\docker" et lancez la commande "docker-compose up -d"

La collection PostMan au format JSON se trouve dans le dossier postman-tests

Lien vers la documentation : https://documenter.getpostman.com/view/11661025/Szzkbc9S
Lien vers le repo git du projet : https://gitlab.com/yanek44/miage-m2-projet_ams
Lien vers la vidéo de présentation : 