/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.gestioncours.repos;

import fr.miage.projet.gestioncours.entities.Cours;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author Yanek
 */
public interface CoursRepository extends MongoRepository<Cours, String>{
}
