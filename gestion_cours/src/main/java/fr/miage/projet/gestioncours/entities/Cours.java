/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.gestioncours.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Yanek
 */
@Getter
@Document
public class Cours {

    public Cours(String idCours, String nom, LocalDate date, String loginEnseignant, String idLieu, Integer dureeMinute, NiveauMaitrise niveau) {
        this();
        this.idCours = idCours;
        this.nom = nom;
        this.date = date;
        this.loginEnseignant = loginEnseignant;
        this.idLieu = idLieu;
        this.dureeMinute = dureeMinute;
        this.niveau = niveau;
    }

    public Cours() {
        this.listeParticipants = new ArrayList<>();
    }

    /**
     * Représente l'id du cours et donc sa clé
     */
    @Id
    String idCours;

    /**
     * Nom du cours
     */
    String nom;

    /**
     * Date a laquelle va se dérouler le cours
     */
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    LocalDate date;

    /**
     * Login de l'enseignant qui dispense le cours
     */
    String loginEnseignant;

    /**
     * Id du lieu ou se déroulera le cours
     */
    String idLieu;

    /**
     * Durée du cours en minutes
     */
    Integer dureeMinute;

    /**
     * Maitrise nécessaire a un membre pour acceder au cours
     */
    NiveauMaitrise niveau;

    /**
     * liste des membres participants au cours
     */
    Collection<String> listeParticipants;
}
