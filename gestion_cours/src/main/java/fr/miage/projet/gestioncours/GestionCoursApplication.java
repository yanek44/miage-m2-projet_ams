package fr.miage.projet.gestioncours;

import fr.miage.projet.gestioncours.entities.Cours;
import fr.miage.projet.gestioncours.entities.NiveauMaitrise;
import fr.miage.projet.gestioncours.repos.CoursRepository;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionCoursApplication {

    private final static Logger LOG = LoggerFactory.getLogger(GestionCoursApplication.class);

    @Autowired
    CoursRepository repo;

    public static void main(String[] args) {
        SpringApplication.run(GestionCoursApplication.class, args);
    }

    //Pour les tests
    @PostConstruct
    public void init() {
        Cours c = new Cours("1", "Natation débutant", LocalDate.of(2021, Month.JUNE, 29), "yanek", "7fca7f7a1c0ddc750e8eb3e4b1128677aec55f0d", 120, NiveauMaitrise.Niveau1);
        c.getListeParticipants().addAll(Arrays.asList("lynch", "choutzi"));
        repo.save(c);        
        repo.save(new Cours("2", "Apprentissage Crawl", LocalDate.of(2021, Month.JULY, 10), "yanek", "7fca7f7a1c0ddc750e8eb3e4b1128677aec55f0d", 60, NiveauMaitrise.Niveau1));
        repo.save(new Cours("3", "Entrainement Régionalles", LocalDate.of(2021, Month.JULY, 23), "yanek", "7fca7f7a1c0ddc750e8eb3e4b1128677aec55f0d", 80, NiveauMaitrise.Niveau3));
        repo.save(new Cours("4", "Crawl : Les meilleures techniques", LocalDate.of(2021, Month.MAY, 12), "yanek", "7fca7f7a1c0ddc750e8eb3e4b1128677aec55f0d", 90, NiveauMaitrise.Niveau2));
        repo.save(new Cours("5", "Le papillon pour les débutants", LocalDate.of(2021, Month.MARCH, 5), "yanek", "7fca7f7a1c0ddc750e8eb3e4b1128677aec55f0d", 20, NiveauMaitrise.Niveau2));
    }
}
