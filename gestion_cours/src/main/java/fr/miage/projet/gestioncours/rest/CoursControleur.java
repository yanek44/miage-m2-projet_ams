/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.gestioncours.rest;

import fr.miage.projet.gestioncours.entities.Cours;
import fr.miage.projet.gestioncours.repos.CoursRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Yanek
 */
@RestController
@RequestMapping("/cours/")
public class CoursControleur {

    private final static Logger LOG = LoggerFactory.getLogger(CoursControleur.class);

    private static final String ERROR_ID_NOT_FOUND = "Impossible de trouver le cours à l'id ";

    @Autowired
    private CoursRepository coursRepository;

    @GetMapping("{idCours}")
    Cours getCoursByLogin(@PathVariable("idCours") String idCours) {
        Optional<Cours> optM = coursRepository.findById(idCours);
        if (optM.isPresent()) {
            LOG.info("getting Membre " + idCours);
            return optM.get();
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + idCours);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + idCours);
        }
    }

    @PostMapping
    void createCours(@RequestBody Cours cours) {
        LOG.info("trying to add new cours :" + cours);
        cours = coursRepository.save(cours);
        LOG.info("Added cours :" + cours);
    }

    @PutMapping("participer/{id}")
    void participerCours(@PathVariable("id") String idCours, @RequestBody String login) {
        Optional<Cours> optM = coursRepository.findById(idCours);
        if (optM.isPresent()) {
            Cours cours = optM.get();
            cours.getListeParticipants().add(login);
            coursRepository.save(cours);
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + idCours);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + idCours);
        }
    }

    /**
     * @return la liste de tous les cours
     */
    @GetMapping
    Collection<Cours> listerCours() {
        Collection<Cours> ret = new ArrayList<>();
        coursRepository.findAll().forEach(ret::add);
        return ret;
    }
}
