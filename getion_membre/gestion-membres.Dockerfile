# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
FROM openjdk:8

ADD target/gestion-membres.jar app.jar
COPY src/main/resources/wait-for-it.sh /usr/wait-for-it.sh
RUN chmod +x /usr/wait-for-it.sh
ENTRYPOINT ["java","-jar","/app.jar"]
