/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.gestionmembres.repos;

import fr.miage.projet.gestionmembres.entities.Membre;
import java.util.Collection;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Yanek
 */
public interface MembreRepository extends CrudRepository<Membre, String>, QuerydslPredicateExecutor<Membre> {
    Collection<Membre> findAll();
    Collection<Membre> findByEnseignant(Boolean enseignant);
    Collection<Membre> findByaPaye(Boolean aPaye);
}
