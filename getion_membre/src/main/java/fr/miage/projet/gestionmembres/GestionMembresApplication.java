package fr.miage.projet.gestionmembres;

import fr.miage.projet.gestionmembres.entities.Membre;
import fr.miage.projet.gestionmembres.entities.NiveauMaitrise;
import fr.miage.projet.gestionmembres.repos.MembreRepository;
import java.time.LocalDate;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class GestionMembresApplication {

    @Autowired
    private MembreRepository repo;

    public static void main(String[] args) {
        SpringApplication.run(GestionMembresApplication.class, args);

    }

    //Pour les tests
    @PostConstruct
    public void init() {
        Membre y = new Membre("yanek", "pwd", "Colonge", "Yanek", "yanek@mail.fr", "1 rue principale");
        y.setAPaye(true);
        y.setCertifMedical(LocalDate.now().minusYears(1).minusDays(1));
        y.setApte(true);
        y.setEnseignant(true);
        repo.save(y);
        repo.save(new Membre("lynch", "pwd", "El Poulino", "Denisse", "denis@mail.fr", "3 rue des lilas"));
        Membre r = new Membre("miagicien", "pwd", "LaPierre", "Rémi", "remi@mail.fr", "27 rue des loies");
        r.setApte(true);
        r.setNiveau(NiveauMaitrise.Niveau3);
        repo.save(r);
        repo.save(new Membre("choutzi", "pwd", "Carrydo", "Sylv", "sylv@mail.fr", "10 boulevard jeanne d'arc"));
    }
}
