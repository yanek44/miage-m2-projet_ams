/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.gestionmembres.rest;

import fr.miage.projet.gestionmembres.entities.Membre;
import fr.miage.projet.gestionmembres.entities.NiveauMaitrise;
import fr.miage.projet.gestionmembres.repos.MembreRepository;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Yanek
 */
@RestController
@RequestMapping("/membres/")
public class MembreControleur {

    private final static Logger LOG = LoggerFactory.getLogger(MembreControleur.class);

    private static final String ERROR_ID_NOT_FOUND = "Impossible de trouver le membre à l'id ";

    @Autowired
    private MembreRepository membreRepository;

    @GetMapping("{login}")
    Membre getMembreByLogin(@PathVariable("login") String login) {
        Optional<Membre> optM = membreRepository.findById(login);
        if (optM.isPresent()) {
            LOG.info("getting Membre " + login);
            return optM.get();
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + login);
        }
    }

    @PostMapping
    void createMembre(@RequestBody Membre newMembre) {
        LOG.info("trying to add new Membre :" + newMembre);
        newMembre = membreRepository.save(newMembre);
        LOG.info("Added Membre :" + newMembre);
    }

    @PutMapping("paiement/{login}")
    void ajouterPaiement(@PathVariable("login") String login, @RequestBody String iban) {
        Optional<Membre> optM = membreRepository.findById(login);
        if (optM.isPresent()) {
            Membre m = optM.get();
            m.setIban(iban);
            membreRepository.save(m);
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + login);
        }
    }

    @PutMapping("payer/{login}")
    void validerPaiement(@PathVariable("login") String login) {
        Optional<Membre> optM = membreRepository.findById(login);
        if (optM.isPresent()) {
            Membre m = optM.get();
            if (m.getIban() == null || m.getIban().equals("")) {
                //change exception
                throw new IllegalStateException();
            }
            m.setAPaye(true);
            membreRepository.save(m);
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + login);
        }
    }

    @PutMapping("certificat/{login}")
    void ajouterCertifMedical(@PathVariable("login") String login) {
        Optional<Membre> optM = membreRepository.findById(login);
        if (optM.isPresent()) {
            Membre m = optM.get();
            m.setCertifMedical(LocalDate.now());
            m.setApte(true);
            membreRepository.save(m);
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + login);
        }
    }

    @PutMapping("licence/{login}")
    void ajouterNumeroLicence(@PathVariable("login") String login, @RequestBody String numLicence) {
        Optional<Membre> optM = membreRepository.findById(login);
        if (optM.isPresent()) {
            Membre m = optM.get();
            m.setNumLicence(numLicence);
            membreRepository.save(m);
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + login);
        }
    }

    @PutMapping("niveau/{login}")
    void modifierNiveauMembre(@PathVariable("login") String login, @RequestParam("niveau") NiveauMaitrise niveau) {
        Optional<Membre> optM = membreRepository.findById(login);
        if (optM.isPresent()) {
            Membre m = optM.get();
            m.setNiveau(niveau);
            membreRepository.save(m);
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + login);
        }
    }

    @PutMapping("enseignant/{login}")
    void devientEnseignant(@PathVariable("login") String login) {
        Optional<Membre> optM = membreRepository.findById(login);
        if (optM.isPresent()) {
            Membre m = optM.get();
            m.setEnseignant(true);
            membreRepository.save(m);
        } else {
            LOG.warn(ERROR_ID_NOT_FOUND + login);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ERROR_ID_NOT_FOUND + login);
        }
    }

    /**
     * @return la liste de tous les membres
     */
    @GetMapping
    Collection<Membre> listerMembres() {
        return membreRepository.findAll();
    }

    /**
     * @return la liste de tous les membres dont l'indicateur "enseignant" est a
     * true
     */
    @GetMapping("enseignants")
    Collection<Membre> listerEnseignants() {
        return membreRepository.findByEnseignant(true);
    }

    /**
     * @return la liste des cotisations qui entrerons dans les caisses de
     * l'association cette année
     */
    @GetMapping("cotisations")
    Collection<Double> listerToutesCotisations() {
        return listerMembres().stream()
                .map(m -> 10d)
                .collect(Collectors.toList());
    }

    /**
     * @return toutes les cotisations deja payées
     */
    @GetMapping("cotisations/payees")
    Collection<Double> listerToutesCotisationsPayees() {
        return membreRepository.findByaPaye(true).stream()
                .map(m -> 10d)
                .collect(Collectors.toList());
    }
}
