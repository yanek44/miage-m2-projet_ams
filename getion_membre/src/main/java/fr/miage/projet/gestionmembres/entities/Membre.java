/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.gestionmembres.entities;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Yanek
 */
@Getter
@Entity
public class Membre {

    /**
     * Représente le login dont se sert l'utilisateur pour se connecter. Aussi
     * la clé dans la base
     */
    
    @Id
    private String login;

    /**
     * Nom du membre
     */
    private String nom;

    /**
     * Prénom du membre
     */
    private String prenom;

    /**
     * Email du membre
     */
    @Column(unique = true)
    private String mail;

    /**
     * Mot de passe du membre
     */
    private String mdp;

    /**
     * Adresse du membre
     */
    private String adresse;

    /**
     * Numéro de licence du membre
     */
    @Column(unique = true)
    @Setter
    private String numLicence;

    /**
     * IBAN pour payer
     */
    @Setter
    private String iban;

    /**
     * Niveau de maitrise du membre
     */
    @Enumerated(EnumType.STRING)
    @Setter
    private NiveauMaitrise niveau;

    /**
     * date de dépot du certificat médicale du membre
     */
    @Setter
    private LocalDate certifMedical;

    /**
     * Vrai si le membre est secrétaire
     */
    @Setter
    private boolean secretaire;

    /**
     * Vrai si le membre est président
     */
    @Setter
    private boolean president;

    /**
     * Vrai si le membre a payé sa cotisation annuelle
     */
    @Setter
    private boolean aPaye;

    /**
     * Vrai si le membre est apte a participer aux activités
     */
    @Setter
    private boolean apte;

    /**
     * Vrai si le membre est enseignant
     */
    @Setter
    private boolean enseignant;

    /**
     * Liste des id des cours enseignés par le membre s'il est enseignant
     */
    @ElementCollection
    private List<Integer> listeCours;

    /**
     * Créer un membre basique
     *
     * @param login
     * @param nom
     * @param prenom
     * @param mail
     * @param mdp
     * @param adresse
     */
    public Membre(String login, String mdp, String nom, String prenom, String mail, String adresse) {
        this();
        this.login = login;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.mdp = mdp;
        this.adresse = adresse;
    }

    public Membre() {
        this.login = "";
        this.nom = "";
        this.prenom = "";
        this.mail = "";
        this.mdp = "";
        this.adresse = "";
        this.numLicence = null;
        this.iban = null;
        this.niveau = NiveauMaitrise.Niveau1;
        this.certifMedical = LocalDate.of(2000, Month.JANUARY, 1);
        this.secretaire = false;
        this.president = false;
        this.aPaye = false;
        this.apte = false;
        this.enseignant = false;
        this.listeCours = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Membre{" + "login=" + login + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", mdp=" + mdp + ", adresse=" + adresse + ", numLicence=" + numLicence + ", iban=" + iban + ", niveau=" + niveau + ", certifMedical=" + certifMedical + ", secretaire=" + secretaire + ", president=" + president + ", aPaye=" + aPaye + ", apte=" + apte + ", enseignant=" + enseignant + ", listeCours=" + listeCours + '}';
    }
}
