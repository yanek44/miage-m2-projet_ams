/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.projet.gestionmembres.scheduled;

import fr.miage.projet.gestionmembres.repos.MembreRepository;
import java.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author Yanek
 */
@Service
public class SheduledTaskMembre {

    private static Logger LOG = LoggerFactory.getLogger(SheduledTaskMembre.class);

    @Autowired
    private MembreRepository membreRepository;

    /**
     * Tous les ans, met à jour les gens qui n’ont pas payé en “En retard de
     * paiement”
     */
    @Scheduled(initialDelay = 10000, fixedDelay = 60000)
    void updateStatutPaiement() {
        membreRepository.findAll().forEach(m -> {
            m.setAPaye(false);
            membreRepository.save(m);
        });
        LOG.info("updating payment status");
    }

    /**
     * Met à jour les gens dont le certificat médical est invalide
     */
    @Scheduled(initialDelay = 10000, fixedDelay = 60000)
    void updateStatutAptitude() {
        membreRepository.findAll()
                .stream()
                .filter(m -> m.isApte() && m.getCertifMedical().plusYears(1).isBefore(LocalDate.now()))
                .forEach(m -> {
            m.setApte(false);
            membreRepository.save(m);
                    LOG.info("Le membre " + m.getLogin() + " n'est plus apte. Date de certif : " + m.getCertifMedical());
                });
    }

}
